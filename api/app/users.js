const express = require('express');
const mongoose = require('mongoose');
const Users = require('../models/User');

const router = express.Router();

router.get('/all',async (req, res, next) => {
  try {
    const users = await Users.find({isOnline: true});
    res.send(users)
  } catch (e) {
    next(e)
  }
});

router.post('/', async (req, res, next) => {
  try {
    const userData = {
      email: req.body.email,
      password: req.body.password,
      name: req.body.name,
    }

    const user = new Users(userData);
    user.generateToken();

    await user.save();

    return res.send(user);

  } catch (e) {
    if (e instanceof mongoose.Error.ValidationError) {
      return res.status(400).send(e);
    }
    return next(e);
  }
});

router.post('/sessions', async (req, res, next) => {
  try {
    await Users.updateOne({email: req.body.email}, {isOnline: true});

    const user = await Users.findOne({email: req.body.email});

    if (!user) {
      return res.status(400).send({error: 'User not found'})
    }

    const isMatch = await user.checkPassword(req.body.password);

    if(!isMatch) {
      return res.status(400).send({error: 'Password is wrong'});
    }

    user.generateToken();

    await user.save();

    return res.send(user);
  } catch (e) {
    if (e instanceof mongoose.Error.ValidationError) {
      return res.status(400).send(e);
    }
    return next(e);
  }
});

router.delete('/sessions', async (req, res, next) => {
  try {
    const token = req.get('Authorization');

    const message = {message: 'OK'};

    if (!token) return res.send(message);

    await Users.updateOne({token: token}, {isOnline: false});
    const user = await Users.findOne({token});

    if (!user) return res.send(message);

    user.generateToken();

    await user.save();

    return res.send(message);
  } catch (e) {
    if (e instanceof mongoose.Error.ValidationError) {
      return res.status(400).send(e);
    }
    return next(e);
  }
});

module.exports = router;