module.exports = {
  mongo: {
    db: 'mongodb://localhost/messenger',
    options: {useNewUrlParser: true},
  }
};