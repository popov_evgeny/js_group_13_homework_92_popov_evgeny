const express = require('express');
const cors = require('cors');
const mongoose = require("mongoose");
const users = require('./app/users');
const config = require('./config');
const app = express();
const Message = require('./models/Message');
const { nanoid } = require('nanoid');
const Users = require("./models/User");
require('express-ws')(app);

const port = 8000;

app.use(cors({origin: 'http://localhost:4200'}));
app.use(express.json());
app.use('/users', users);

const activeConnections = {};

app.ws('/chat', function (ws, req) {

  const id = nanoid();
  activeConnections[id] = ws;

  let user = {
    username: String,
    token: String,
    time: String
  }

  ws.on('message', async (msg) => {
    const decodedMessage = JSON.parse(msg);
    if (decodedMessage.type === 'SEND_MESSAGE') {
      let msgData = {
        user: user.username,
        message: decodedMessage.message
      }

      const messages = new Message(msgData);

      await messages.save();
    }

    switch (decodedMessage.type) {
      case 'SET_USER':
        user.username = decodedMessage.username;
        user.token = decodedMessage.token;
        let msgData = {
          user: user.username,
          message: 'Connect!'
        }

        const message = new Message(msgData);

        await message.save();

        const messages = await Message.find().limit(30);

        Object.keys(activeConnections).forEach(connId => {
          const conn = activeConnections[connId];
          conn.send(JSON.stringify({
            type: 'NEW_MESSAGE',
            message: messages
          }));
        });
        break;
      case 'SEND_MESSAGE':
        if (user.token === '') {
          break;
        }
        const msg = await Message.find();

        Object.keys(activeConnections).forEach(connId => {
          const conn = activeConnections[connId];
          conn.send(JSON.stringify({
            type: 'NEW_MESSAGE',
            message: msg
          }));
        });
        break;
      default:
    }
  });

  ws.on('close', async () => {
    let msgData = {
      user: user.username,
      message: 'Disconnected!'
    }

    await Users.updateOne({token: user.token}, {isOnline: false});

    const message = new Message(msgData);

    await message.save();

    const messages = await Message.find();

    Object.keys(activeConnections).forEach(connId => {
      const conn = activeConnections[connId];
      conn.send(JSON.stringify({
        type: 'NEW_MESSAGE',
        message: messages
      }));
    });
    delete activeConnections[id];
  });
});

const run = async () => {
  await mongoose.connect(config.mongo.db, config.mongo.options);

  app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
  });

  process.on('exit', () => {
    mongoose.disconnect();
  });
};

run().catch(e => console.error(e));