import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { Observable } from 'rxjs';
import { User, Users } from '../../models/user.model';
import { fetchUsersRequest } from '../../store/users.actions';


interface Message {
  user: string,
  message: string
}

interface ServerMessage {
  type: string,
  message: Message
}

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.sass']
})
export class ChatComponent implements OnInit, OnDestroy {
  userName!: string;
  userToken!: string;
  messageText = '';
  messages: Message[] = [];
  ws!: WebSocket;
  user: Observable<null | User>;
  users: Observable<Users[]>

  constructor(
    private store: Store<AppState>,

  ) {
    this.user = store.select(state => state.users.user);
    this.users = store.select(state => state.users.users);
  }

  ngOnInit(): void {
    this.ws = new WebSocket('ws://localhost:8000/chat');
    
    this.ws.onclose = () => {
      setTimeout(() => {
        this.ws = new WebSocket('ws://localhost:8000/chat');
      });
    }

    this.ws.onmessage = event => {
      const decodedMessage: ServerMessage = JSON.parse(event.data);

      if (decodedMessage.type === 'NEW_MESSAGE') {
        this.messages.length = 0;
        this.messages = this.messages.concat(decodedMessage.message);
        this.store.dispatch(fetchUsersRequest());
      }
    };
    this.user.subscribe( user => {
      this.userName = <string>user?.name;
      this.userToken = <string>user?.token;
    })

    this.ws.onopen = () => {
      this.setUsername();
    }
  }


  setUsername() {
    this.ws.send(JSON.stringify({
      type: 'SET_USER',
      username: this.userName,
      token: this.userToken
    }));
  }

  sendMessage() {
      this.ws.send(JSON.stringify({
        type: 'SEND_MESSAGE',
        message: this.messageText
      }));

    this.messageText = '';
  }

  ngOnDestroy() {
    this.ws.close();
  }

}
