import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApiUsers, LoginUserData, RegisterUserData, User, Users } from '../models/user.model';
import { environment as env } from '../../environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  constructor(private http: HttpClient) { }

  getUsers() {
    return this.http.get<ApiUsers[]>(env.apiUrl + '/users/all' ).pipe(
      map(response => {
        return response.map( users => {
          return new Users(
            users.name
          );
        });
      })
    );
  }

  registerUser(userData: RegisterUserData) {
    return this.http.post<User>(env.apiUrl + '/users', userData);
  }

  login(userData: LoginUserData) {
    return this.http.post<User>(env.apiUrl + '/users/sessions', userData);
  }

  logout(token: string) {
    return this.http.delete(env.apiUrl + '/users/sessions',
      {headers: new HttpHeaders({'Authorization': token})});
  }
}
