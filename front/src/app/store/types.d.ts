import { LoginError, RegisterError, User, Users } from '../models/user.model';


export type UserState = {
  user: null | User,
  users: Users[],
  fetchLoading: boolean,
  fetchError: null | string,
  registerLoading: boolean,
  registerError: null | RegisterError,
  loginLoading: boolean,
  loginError: null | LoginError
};


export type AppState = {
  users: UserState,
}
